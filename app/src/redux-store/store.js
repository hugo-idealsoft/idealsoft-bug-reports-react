import { createStore, applyMiddleware, combineReducers } from 'redux';
import ReduxPromise from 'redux-promise';

import NewBugReducer from '../screens/bugs/newbug/NewBugReducer';
import { reducer as formReducer } from 'redux-form';

const reducer = combineReducers({ newBug: NewBugReducer, form: formReducer });
const store = createStore(reducer, applyMiddleware(ReduxPromise));

export default store;
