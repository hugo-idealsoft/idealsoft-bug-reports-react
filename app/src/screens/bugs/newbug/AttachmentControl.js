import React from 'react';
import Dropzone from 'react-dropzone';

class AttachmentControl extends React.Component {
  onDrop = (acceptedFiles) => {
    const {
      input: { value, onChange },
    } = this.props;

    let novosAnexos = value.concat(
      acceptedFiles.map((element) => {
        return {
          nome: element.name,
          tamanho: element.size,
          criadoEm: new Date(),
        };
      })
    );

    console.log('novosAnexos', novosAnexos);

    onChange(novosAnexos);
  };

  render() {
    const {
      input: { value },
    } = this.props;

    return (
      <div>
        <label>
          Anexos
          <Dropzone onDrop={this.onDrop.bind(this)}>
            {({ getRootProps, getInputProps, isDragActive }) => (
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                {isDragActive
                  ? 'Solte o arquivo...'
                  : 'Clique para adicionar ou arraste um arquivo...'}
              </div>
            )}
          </Dropzone>
          <div>
            <ul>
              {value &&
                value.map((f) => {
                  return (
                    <li key={f.criadoEm.toLocaleTimeString()}>
                      Arquivo: {f.nome} Tamanho: {f.tamanho / 1024} KB Criado
                      em: {f.criadoEm.toLocaleString()}
                    </li>
                  );
                })}
            </ul>
          </div>
        </label>
      </div>
    );
  }
}

export default AttachmentControl;
