import React from 'react';
import { connect } from 'react-redux';

import {
  enviaBug,
  listaStatus,
  listaUsuarios,
  carregandoDados,
} from './NewBugActions';

import NewBugControl from './NewBugControl';

class NewBug extends React.Component {
  async componentDidMount() {
    const { carregarDados } = this.props;

    carregarDados();
  }

  handleSubmit = (data) => {
    const { enviaBug } = this.props;

    enviaBug({
      id: data.bugID,
      titulo: data.titulo,
      data: data.data,
      status: data.status,
      lancadoPor: data.lancadoPor,
      testador: data.testador,
      anexos: data.anexos,
      descricao: data.descricao,
      interessados: data.interessados,
    });

    alert(`Bug postado! 
  
    id ${data.bugID}, 
    titulo ${data.titulo},
    data ${data.data},
    status ${data.status},
    lancadoPor ${data.lancadoPor},
    testador ${data.testador},
    anexos ${data.anexos},
    descricao ${data.descricao},
    interessados ${data.interessados},`);
  };

  initialValues = (usuarios, status) => {
    return {
      bugID: '',
      titulo: '',
      data: '',
      status: status && status.length ? status[0].status : [''],
      lancadoPor: usuarios && usuarios.length ? usuarios[0].nomeUsuario : [''],
      testador:
        usuarios && usuarios.filter((t) => t.testador).length
          ? usuarios.filter((t) => t.testador)[0].nomeUsuario
          : [''],
      anexos: [],
      descricao: '',
      interessados: '',
    };
  };

  render() {
    const { usuarios, status, carregandoDados } = this.props;

    if (carregandoDados) return <h1>Carregando...</h1>;

    return (
      <NewBugControl
        onSubmit={this.handleSubmit.bind(this)}
        initialValues={this.initialValues(usuarios, status)}
        usuarios={usuarios}
        status={status}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.newBug.data,
    errors: state.newBug.errors,
    usuarios: state.newBug.usuarios,
    status: state.newBug.status,
    carregandoDados: state.newBug.carregandoDados,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    enviaBug: () => dispatch(enviaBug()),

    carregarDados: () => {
      dispatch(carregandoDados(true));

      dispatch(listaUsuarios());
      dispatch(listaStatus());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewBug);
