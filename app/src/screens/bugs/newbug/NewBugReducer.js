import {
  ENVIA_BUG,
  LISTA_STATUS,
  LISTA_USUARIOS,
  CARREGANDO_DADOS,
} from './NewBugActions';

const initialState = {
  errors: null,
  data: null,
  usuarios: null,
  status: null,
  carregandoDados: true
};

export default function (state = initialState, action) {
  const { type, payload } = action; //desconstruct

  switch (type) {
    case ENVIA_BUG:
      // console.log('***NewBugReducer.ENVIA_BUG.payload', payload);

      return {
        ...state,
        data: payload,
      };

    case LISTA_STATUS:
      // console.log('***NewBugReducer.GET_STATUS.payload', payload);

      return {
        ...state,
        errors: payload && payload.errors,
        status: payload && payload.result.status.map((f) => f.value),
        carregandoDados: !state.usuarios && !state.status,
      };
    case LISTA_USUARIOS:
      // console.log('***NewBugReducer.GET_USUARIOS.payload', payload);

      return {
        ...state,
        errors: payload && payload.errors,
        usuarios: payload && payload.result.usuarios.map((f) => f.value),
        carregandoDados: !state.usuarios && !state.status,
      };
    case CARREGANDO_DADOS:
      return {
        ...state,
        carregandoDados: payload,
      };
    default:
      return state;
  }
}
