import React from 'react';
import { reduxForm, Field } from 'redux-form';

import AttachmentControl from './AttachmentControl';
import { connect } from 'react-redux';

class NewBugControl extends React.Component {
  componentDidMount() {}

  componentWillUnmount() {}

  componentDidUpdate() {}

  addAnexos(anexos) {
    this.props.change('anexos', anexos);
  }

  renderField = ({
    input,
    label,
    type,
    disabled,
    meta: { touched, error, warning },
    values,
  }) => (
    <div>
      <label>{label}</label>
      <div>
        {type !== 'select' && (
          <input
            {...input}
            placeholder={label}
            type={type}
            disabled={disabled}
          />
        )}
        {type === 'select' && values && (
          <select {...input}>
            {values &&
              values.map((u, index) => (
                <option key={index} value={u}>
                  {u}
                </option>
              ))}
          </select>
        )}
        {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  );

  render() {
    const { reset, handleSubmit, usuarios, status } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <label>
            Bug ID
            <Field
              component={this.renderField}
              type='text'
              className='form-control'
              name='bugID'
              props={{ disabled: true }}
            />

            <button className='form-control' type='button' value='Próximo Código' />
          </label>
        </div>

        <div className='form-group'>
          <label>
            Titulo
            <Field
              component={this.renderField}
              type='text'
              className='form-control'
              name='titulo'
            />
          </label>
        </div>

        <div className='form-group'>
          <label>
            Data
            <Field
              component={this.renderField}
              className='form-control'
              type='date'
              name='data'
            />
          </label>
        </div>

        <div className='form-group'>
          <label>
            Status
            <Field
              component={this.renderField}
              values={status.map((s) => s.status)}
              name='status'
              type='select'
            ></Field>
          </label>
        </div>

        <div className='form-group'>
          <label>
            Lançado Por
            <Field
              name='lancadoPor'
              component={this.renderField}
              values={usuarios.map((u) => u.nomeUsuario)}
              type='select'
            ></Field>
          </label>
        </div>

        <div className='form-group'>
          <label>
            Testador
            <Field
              name='testador'
              component={this.renderField}
              values={usuarios
                .filter((t) => t.testador)
                .map((u) => u.nomeUsuario)}
              type='select'
            ></Field>
          </label>
        </div>

        <div className='form-group'>
          <Field
            name='anexos'
            component={AttachmentControl}
            onChange={(data) => this.addAnexos(data)}
          />
        </div>

        <div className='form-group'>
          <label>
            Descrição
            <Field
              className='form-control'
              name='descricao'
              component={this.renderField}
              type='textarea'
            />
          </label>
        </div>

        <div className='form-group'>
          <label>
            Interessados
            <Field
              component={this.renderField}
              type='text'
              className='form-control'
              name='interessados'
            />
          </label>
        </div>

        <button className='form-control' type='submit'>
          Gravar
        </button>
        <button className='form-control' type='button' onClick={reset}>
          Limpar
        </button>
      </form>
    );
  }
}

NewBugControl = reduxForm({
  form: 'newbugcontrol',
  fields: [
    'bugID',
    'titulo',
    'data',
    'status',
    'lancadoPor',
    'testador',
    'anexos',
    'descricao',
    'interessados',
  ],
})(NewBugControl);

export default connect()(NewBugControl);
