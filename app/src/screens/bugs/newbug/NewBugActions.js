import {
  postBug,
  getUsuarios,
  getStatus,
} from '../../../helpers/BugReportsApi';

export const ENVIA_BUG = 'ENVIA_BUG';

export const LISTA_USUARIOS = 'LISTA_USUARIOS';
export const LISTA_STATUS = 'LISTA_STATUS';

export const CARREGANDO_DADOS = 'CARREGANDO_DADOS';

export const enviaBug = (bug) => {
  const payload = postBug(bug);

  //console.log('***NewBugAction.enviaBug.payload', payload);

  return {
    type: ENVIA_BUG,
    payload,
  };
};

export const listaUsuarios = () => {
  const payload = getUsuarios();

  // console.log('***NewBugAction.listaUsuarios.payload', payload);

  return {
    type: LISTA_USUARIOS,
    payload,
  };
};

export const listaStatus = () => {
  const payload = getStatus();

  // console.log('***NewBugAction.listaStatus.payload', payload);

  return {
    type: LISTA_STATUS,
    payload,
  };
};

export const carregandoDados = (carregando) => {
  const payload = carregando;

  console.log('***NewBugAction.carregandoDados.payload', payload);

  return {
    type: CARREGANDO_DADOS,
    payload,
  };
};
