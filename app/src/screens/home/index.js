import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';

import ListBug from '../bugs/listbug/';

import NewBug from '../bugs/newbug/NewBug';
import Clock from '../common/Clock';

const Home = () => {
  return (
    <div>
      <div>
        <h1>Bug reports</h1>

        <BrowserRouter>
          <div>
            <Link to='/newbug'>Novo Bug</Link>
            <Link to='/listbug'>Listar Bug</Link>
            <Link to='/populatelist'>Popular Lista</Link>
          </div>

          <Clock />

          <hr />

          <Switch>
            <Route path='/newbug/'>
              <NewBug />
            </Route>
            <Route path='/listbug/'>
              <ListBug />
            </Route>
            <Route path='/populatelist/'></Route>
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
};

export default Home;
