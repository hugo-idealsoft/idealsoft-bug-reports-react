import { doGet, doPost } from './ApiHelper';

export const getBugs = async (pagina) =>
  await doGet(`/bugs/?pagina=${parseInt(pagina)}`);

export const getBug = async (id) => await doGet(`/bugs/${parseInt(id)}`);

export const countBugs = async () => await doGet('/bugs/count');

export const postBug = async (obj) => {
  await doPost('/bugs/', obj);

  return true;
};

export const getUsuarios = () => doGet('/cadastros/usuarios/');

export const getStatus = () => doGet('/cadastros/status/');

export const getUsuario = (id) => doGet(`/cadastros/usuarios/${parseInt(id)}`);
