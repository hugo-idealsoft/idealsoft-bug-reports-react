const API_URL = 'api';

const getAPiWithPath = (path) => `${API_URL}${path}`;
const getBasicPropsFromFetch = (method, obj) => ({
  method: method,
  body: JSON.stringify(obj),
  headers: {
    'Content-Type': 'application/json',
  },
});

export const doGet = (path) => {
  const url = getAPiWithPath(path);

  return fetch(url).then((response) => response.json());
};

export const doPost = (path, obj) => {
  const url = getAPiWithPath(path);
  const params = getBasicPropsFromFetch('POST', obj);

  return fetch(url, params).then((response) => response.json());
};

export const doPut = (path, obj) => {
  const url = getAPiWithPath(path);
  const params = getBasicPropsFromFetch('PUT', obj);

  return fetch(url, params).then((response) => response.json());
};
