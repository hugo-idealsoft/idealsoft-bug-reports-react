# Bug Reports APP (React)

Contém o app react baseado no comando
```
npx create-react-app app
```

Para rodar:

- Instalar as dependencias locais:
```
npm install
```

- (Opcional) Criar arquivo .env para definir as propriedades de configuração do projeto. Devem constar as seguintes variáveis:
```
PORT=(porta que a API vai rodar)
REACT_APP_API_URL=(URL da API)
```

- Iniciar o projeto
```
npm start
```

- Libs adicionadas a este projeto

```
npm install --save redux
npm install --save react-redux
npm install --save redux-form
npm install --save react-dropzone
npm install --save redux-promise
```