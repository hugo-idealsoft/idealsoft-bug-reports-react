const { getUsuario, getUsuarios } = require('../services/couchdb/cadastroConn');

exports.get = async (req, res, next) => {
  const items = await getUsuarios();

  res.status(200).send(items);
};

exports.getid = async (req, res, next) => {
  const { id } = req.params;
  const items = await getUsuario(id);

  res.status(200).send(items);
};

exports.get = async (req, res, next) => {
  const items = await getUsuarios();

  res.status(200).send(items);
};
