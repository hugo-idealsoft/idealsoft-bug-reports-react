const { getDoc, createDoc } = require('../services/couchdb/anexoConn');

exports.getid = async (req, res, next) => {
  const id = req.params.id;
  const items = await getDoc(id);
  res.status(200).send(items);
};

exports.post = async (req, res, next) => {
  await createDoc(req.body);
  res.status(201).send();
};
