const { getStatus } = require('../services/couchdb/cadastroConn');

exports.get = async (req, res, next) => {
  const items = await getStatus();

  res.status(200).send(items);
};
