const { getbyBugId, createDoc } = require('../services/couchdb/descricaoConn');

exports.getbyBugId = async (req, res, next) => {
  const id = req.params.id;
  const items = await getbyBugId(id);
  res.status(200).send(items);
};
