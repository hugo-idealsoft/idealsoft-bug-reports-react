// exports.post = (req, res, next) => {
//     res.status(201).send('Requisição recebida com sucesso!');
// };

// exports.put = (req, res, next) => {
//     let id = req.params.id;
//     res.status(201).send(`Requisição recebida com sucesso! ${id}`);
// };

// exports.delete = (req, res, next) => {
//     let id = req.params.id;
//     res.status(200).send(`Requisição recebida com sucesso! ${id}`);
// };
//const cosmoConn = require("../services/cosmoConn");
const {
  getAll,
  getDoc,
  createDoc,
  countBugs,
} = require('../services/couchdb/bugsConn');

exports.get = async (req, res, next) => {
  const { pagina } = req.query;
  const items = await getAll(pagina);

  res.status(200).send(items);
};

exports.getid = async (req, res, next) => {
  const { id } = req.params;
  const items = await getDoc(id);

  res.status(200).send(items);
};

exports.count = async (req, res, next) => {
  const result = await countBugs(); //desconstruct

  res.status(200).send(result);
};

exports.post = async (req, res, next) => {
  await createDoc(req.body);

  res.status(201).send();
};
