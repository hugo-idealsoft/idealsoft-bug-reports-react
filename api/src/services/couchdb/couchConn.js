const config = require("../../config");

const dbname = 'bugreports';
const endpoint = `http://${config.db.user}:${config.db.pass}@${config.db.url}:${config.db.port}`;

const nano = require('nano')(endpoint);

async function createOrUse() {
    try {
        const test = await nano.db.get(dbname);
        if (test) return nano.use(dbname);

        await nano.db.create(dbname);
        return nano.use(dbname);
    } catch {
        await nano.db.clscreate(dbname);
        return nano.use(dbname);
    }
}

async function createDoc(doc) {
    // const bugreports = await createOrUse();
    // const response = await bugreports.insert(doc)

    console.log(doc);
    
    return true;

    // return response.ok; 
}

module.exports.createOrUse = createOrUse;
module.exports.createDoc = createDoc;