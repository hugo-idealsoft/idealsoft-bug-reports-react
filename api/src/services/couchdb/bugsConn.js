const { createOrUse, createDoc } = require('./couchConn');

async function getDoc(id) {
  const bugreports = await createOrUse();
  const items = await bugreports.view('bugs', 'bugsSimples', {
    key: parseInt(id),
  });

  return {
    errors: null,
    result: items.rows.length ? items.rows[0].value : null,
  };
}

async function getAll(page) {
  const pageLimit = 25;
  const bugreports = await createOrUse();
  let props = { limit: pageLimit };

  const { count } = await countBugs(); //desconstruct

  if (page) {
    const currentPageSkip = (page - 1) * pageLimit;
    props = { limit: pageLimit, skip: currentPageSkip };
  }

  const items = await bugreports.view('bugs', 'bugsSimples', props);
  const bugs = items.rows.map((doc) => {
    return doc.value;
  });

  return {
    errors: null,
    result: {
      count: count,
      page: parseInt(page),
      pages: count / pageLimit,
      bugs,
    },
  };
}

async function countBugs() {
  const bugreports = await createOrUse();
  const items = await bugreports.view('bugs', 'countBugsSimples');

  const countBugs = items.rows.map((doc) => doc.value).reduce((t, n) => t + n);

  return { count: countBugs };
}

module.exports.createDoc = createDoc;
module.exports.getAll = getAll;
module.exports.getDoc = getDoc;
module.exports.countBugs = countBugs;
