const { createOrUse, createDoc } = require("./couchConn");

async function getDoc(id) {
    const q = {
        selector: {
            anexoid: { "$eq": parseInt(id) }
        },
        limit: 1
    };

    const bugreports = await createOrUse();
    const item = await bugreports.find(q);

    return item.docs[0];
}

module.exports.createDoc = createDoc;
module.exports.getDoc = getDoc;