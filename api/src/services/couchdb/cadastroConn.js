const { createOrUse } = require('./couchConn');

async function getUsuario(id) {
  const bugreports = await createOrUse();
  const items = await bugreports.view('cadastros', 'usuarios', {
    key: parseInt(id),
  });

  return {
    errors: null,
    result: items.rows.length ? items.rows[0].value : null,
  };
}

async function getUsuarios() {
  const bugreports = await createOrUse();
  const items = await bugreports.view('cadastros', 'usuarios');

  return {
    errors: null,
    result: {
      count: items.rows.length,
      page: 0,
      pages: 0,
      usuarios: items.rows,
    },
  };
}

async function getStatus() {
  const bugreports = await createOrUse();
  const items = await bugreports.view('cadastros', 'status');

  return {
    errors: null,
    result: {
      count: items.rows.length,
      page: 0,
      pages: 0,
      status: items.rows,
    },
  };
}

module.exports.getUsuario = getUsuario;
module.exports.getUsuarios = getUsuarios;
module.exports.getStatus = getStatus;
