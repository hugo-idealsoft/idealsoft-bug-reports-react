const { createOrUse, createDoc } = require("./couchConn");

async function getbyBugId(id) {
    const bugreports = await createOrUse();
    const items = await bugreports.view('bugs', 'descricoes', { key: parseInt(id),  })
    if (items.rows.length == 0) return null;
    
    return items.rows.map((doc) => {
        return doc.value;
    });
}

module.exports.createDoc = createDoc;
module.exports.getbyBugId = getbyBugId;