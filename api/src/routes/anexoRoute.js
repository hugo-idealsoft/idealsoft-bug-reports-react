const express = require('express');
const router = express.Router();
const controller = require('../controllers/anexoController')

router.get('/:id', controller.getid);
router.post('/', controller.post);

module.exports = router;