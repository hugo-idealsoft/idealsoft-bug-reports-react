const express = require('express');
const router = express.Router();
const controller = require('../controllers/bugController')

router.get('/', controller.get);
router.get('/count', controller.count);
router.get('/:id', controller.getid);
router.post('/', controller.post);
// router.put('/:id', controller.put);
// router.delete('/:id', controller.delete);

module.exports = router;