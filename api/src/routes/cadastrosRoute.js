const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarioController');
const statusController = require('../controllers/statusController');

router.get('/usuarios/', usuarioController.get);
router.get('/usuarios/:id', usuarioController.getid);

router.get('/status/', statusController.get);

module.exports = router;
