const express = require('express');
const router = express.Router();
const controller = require('../controllers/descricaoController')

router.get('/:id', controller.getbyBugId); 

module.exports = router;