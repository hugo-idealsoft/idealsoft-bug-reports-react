const express = require('express');
const app = express();
const router = express.Router();

//Rotas
const index = require('./routes/index');
const bugsRoute = require('./routes/bugRoute');
const anexoRoute = require('./routes/anexoRoute');
const descricaoRoute = require('./routes/descricaoRoute');
const cadastroRoute = require('./routes/cadastrosRoute');

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use('/', index);
app.use('/api/bugs', bugsRoute);
app.use('/api/anexo', anexoRoute);
app.use('/api/descricao', descricaoRoute);
app.use('/api/cadastros', cadastroRoute);

module.exports = app;
