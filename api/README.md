# Bug Reports API

Contém o server em node.js com a API baseada em Express.js para fazer a comunicação com o app.
Para rodar:

- Instalar as dependencias globais:
```
npm install nodemon -g
```
O pacote nodemon permite que você rode o server com recuperação de erro e recarregando a cada vez que você alterar um arquivo, o que facilita muito o desenvolvimento.

- Instalar as dependencias locais:
```
npm install
```

- (Opcional) Criar arquivo .env para definir as propriedades de configuração do projeto. Devem constar as seguintes variáveis:
```
PORT=(porta que a API vai rodar)
DB_URL=(URL do banco de dados - atualmente o COSMODB)
DB_APIKEY=(Chave da API do COSMODB - pode mudar e se tornar obsoleto no futuro)
```

- Iniciar o projeto
```
npm start
```