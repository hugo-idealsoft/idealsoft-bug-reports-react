﻿using FizzWare.NBuilder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Windows;

namespace GeradorDadosBugReports
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string BaseCouchDbApiAddress;
        private static HttpClient Client = new HttpClient();
        private static string AuthCouchDbCookieKeyName = "AuthSession";
        private static CookieContainer CookieContainer = new CookieContainer();
        private static string UsuarioLogin;
        private static string SenhaLogin;
        private static int QuantidadeGerar;
        private static string CaminhoBanco;
        private static string[] Palavras;


        private static string[] Status = new[] { "Lançado", "Em estudo", "Bug lançado", "Corrigido", "Negado" };
        private static string[] Modulos = new[] { "Cadastro", "Movimento", "Financeiro", "Relatórios", "Módulo Externo" };
        private static Usuario[] Usuarios = new[]
        {
            new Usuario() {Codigo_Usuario = 1, Nome_Usuario = "Adnre"},
            new Usuario() {Codigo_Usuario = 2, Nome_Usuario = "Caytlon"},
            new Usuario() {Codigo_Usuario = 3, Nome_Usuario = "Cluadia"},
            new Usuario() {Codigo_Usuario = 4, Nome_Usuario = "Doige"},
            new Usuario() {Codigo_Usuario = 5, Nome_Usuario = "Sarcom"},
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Clicou_Botao_Gerar(object sender, RoutedEventArgs e)
        {
            try
            {
                UsuarioLogin = UsuarioTela.Text;
                SenhaLogin = SenhaTela.Text;
                CaminhoBanco = Caminho_Banco.Text;
                QuantidadeGerar = System.Convert.ToInt32(QuantidadeTela.Text);

                Palavras = System.IO.File.ReadAllLines("wordlist.txt");
                BugReportsGeraInformacoes();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        public static string GetAuthenticationCookie(Credentials credentials)
        {
            Client = new HttpClient() { BaseAddress = new Uri(BaseCouchDbApiAddress) };
            string authPayload = JsonConvert.SerializeObject(credentials);
            var authResult = Client.PostAsync("/_session", new StringContent(authPayload, Encoding.UTF8, "application/json")).Result;
            if (authResult.IsSuccessStatusCode)
            {
                var responseHeaders = authResult.Headers.ToList();
                string plainResponseLoad = authResult.Content.ReadAsStringAsync().Result;
                Console.WriteLine("Authenticated user from CouchDB API:");
                Console.WriteLine(plainResponseLoad);
                var authCookie = responseHeaders.Where(r => r.Key == "Set-Cookie").Select(r => r.Value.ElementAt(0)).FirstOrDefault();
                if (authCookie != null)
                {
                    int cookieValueStart = authCookie.IndexOf("=") + 1;
                    int cookieValueEnd = authCookie.IndexOf(";");
                    int cookieLength = cookieValueEnd - cookieValueStart;
                    string authCookieValue = authCookie.Substring(cookieValueStart, cookieLength);
                    return authCookieValue;
                }
                throw new Exception("There is auth cookie header in the response from the CouchDB API");
            }

            throw new HttpRequestException(string.Concat("Authentication failure: ", authResult.ReasonPhrase));
        }

        public static void BugReportsGeraInformacoes()
        {

            Credentials couchDbCredentials = new Credentials()
            {
                //Username = "admin",
                //Password = "senha"
                Username = UsuarioLogin,
                Password = SenhaLogin
            };
            var i = 0;
            var qtdePorPagina = 5000;
            var maxPaginas = (int)Math.Ceiling(((decimal)QuantidadeGerar / (decimal)qtdePorPagina));
            var resto = QuantidadeGerar;
            foreach (var pagina in Enumerable.Range(1, maxPaginas))
            {
                BaseCouchDbApiAddress = CaminhoBanco;
                Client = new HttpClient() { BaseAddress = new Uri(BaseCouchDbApiAddress) };

                string authCookie = GetAuthenticationCookie(couchDbCredentials);
                CookieContainer.Add(new Uri(BaseCouchDbApiAddress), new Cookie(AuthCouchDbCookieKeyName, authCookie));

                var qtderegs = qtdePorPagina;
                resto -= qtderegs;
                if (resto > 0 && resto < qtderegs) qtderegs = resto;

                var ListaBugs = Builder<Bug>.CreateListOfSize(qtderegs).All()
                .With(c => c.BugID = ++i)
                .With(c => c.Titulo = Faker.StringFaker.Alpha(25))
                .With(c => c.Data = Faker.DateTimeFaker.DateTime(new DateTime(2019, 1, 1), DateTime.Now))
                .With(c => c.Status = Faker.NumberFaker.Number(4))
                .With(c => c.Anexos = new List<Anexos>())
                .With(c => c.Lancado_Por = Pick<Usuario>.RandomItemFrom(Usuarios))
                .With(c => c.Testador = Pick<Usuario>.RandomItemFrom(Usuarios))
                .With(c => c.Interessados = Enumerable.Range(0, Faker.NumberFaker.Number(0, 2)).Select(x => Pick<Usuario>.RandomItemFrom(Usuarios)).ToList())
                .With(c => c.Descricao = Enumerable.Range(0, Faker.NumberFaker.Number(0, 4)).Select(x =>
                 {
                     var u = Pick<Usuario>.RandomItemFrom(Usuarios);

                     return new Descricao()
                     {
                         Codigo_Usuario = u.Codigo_Usuario,
                         Nome_Usuario = u.Nome_Usuario,
                         Data = Faker.DateTimeFaker.DateTime(new DateTime(2019, 1, 1), DateTime.Now),
                         texto = GerarDescricao(Faker.NumberFaker.Number(50, 5000))
                     };
                 }).ToList())
                .Build();

                foreach (var bug in ListaBugs)
                {
                    string vaiBug = JsonConvert.SerializeObject(bug);

                    var result = Client.PostAsync("/bugreports", new StringContent(vaiBug, Encoding.UTF8, "application/json")).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        Console.WriteLine(result.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        Console.WriteLine(result.ReasonPhrase);
                    }
                }

                Client.Dispose();

            }

        }

        private static string GerarDescricao(int qtdePalavras)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < qtdePalavras; i++)
            {
                var prox = Faker.NumberFaker.Number(0, Palavras.Length - 1);
                sb.Append($"{Palavras[prox]} ");
            }
            return sb.ToString();
        }

        public class Credentials
        {
            [JsonProperty(PropertyName = "username")]
            public string Username { get; set; }

            [JsonProperty(PropertyName = "password")]
            public string Password { get; set; }
        }

        public class Bug
        {
            [JsonProperty(PropertyName = "bugid")]
            public int BugID { get; set; }

            [JsonProperty(PropertyName = "titulo")]
            public string Titulo { get; set; }

            [JsonProperty(PropertyName = "data")]
            public DateTime Data { get; set; }

            [JsonProperty(PropertyName = "status")]
            public int Status { get; set; }

            [JsonProperty(PropertyName = "lancadopor")]
            public Usuario Lancado_Por { get; set; }

            [JsonProperty(PropertyName = "testador")]
            public Usuario Testador { get; set; }

            [JsonProperty(PropertyName = "anexos")]
            public List<Anexos> Anexos { get; set; }

            [JsonProperty(PropertyName = "descricao")]
            public List<Descricao> Descricao { get; set; }

            [JsonProperty(PropertyName = "interessados")]
            public List<Usuario> Interessados { get; set; }
        }

        public class Usuario
        {
            [JsonProperty(PropertyName = "codigousuario")]
            public int Codigo_Usuario { get; set; }

            [JsonProperty(PropertyName = "nomeusuario")]
            public string Nome_Usuario { get; set; }
        }

        public class Anexos
        {
            [JsonProperty(PropertyName = "codigousuario")]
            public int Codigo_Usuario { get; set; }

            [JsonProperty(PropertyName = "nomeusuario")]
            public string Nome_Usuario { get; set; }

            [JsonProperty(PropertyName = "grupo")]
            public int Grupo { get; set; }

            [JsonProperty(PropertyName = "data")]
            public DateTime Data { get; set; }

            [JsonProperty(PropertyName = "anexoId")]
            public int AnexoId { get; set; }

            [JsonProperty(PropertyName = "filename")]
            public string FileName { get; set; }

            [JsonProperty(PropertyName = "anexo")]
            public string Anexo { get; set; }
        }

        public class Descricao
        {
            [JsonProperty(PropertyName = "codigousuario")]
            public int Codigo_Usuario { get; set; }

            [JsonProperty(PropertyName = "nomeusuario")]
            public string Nome_Usuario { get; set; }

            [JsonProperty(PropertyName = "grupo")]
            public int Grupo { get; set; }

            [JsonProperty(PropertyName = "data")]
            public DateTime Data { get; set; }

            [JsonProperty(PropertyName = "texto")]
            public string texto { get; set; }

        }
    }

}
